<!DOCTYPE html>
<html lang="en">
<head>
  <title>Create USer</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js" ></script>

</head>
<body>
@if (Session()->has('done'))
        <script>
            swal("Doone!", "User Successfully Created!", "success");
        </script>
@endif
<div class="container">
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              {{-- <h4 class="modal-title float-right">Enter Email</h4> --}}
            </div>
            <div class="modal-body">
            <form id="email_found_form">
                @csrf
              <p>Enter email if you already entered the data.</p>
              <input type="email" name="modal_email" class="form-control" id="modal_email">
              <label style="color: red ;display:none" id="not_found_message">Email Not found</label>
              <br>
              <button class="btn btn-primary float-right ml-1" type="submit" id="search_email">Search</button>
              <button type="button" class="btn btn-secondary float-right" data-dismiss="modal">Close</button>

            </form>
            </div>
            <div class="modal-footer">
            </div>
          </div>

        </div>
      </div>

  <h2>User Create</h2>
  <form id="user_information_form">
    @csrf
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="email">Email:</label>
                <input type="text" class="form-control" id="email"  name="email">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="father_name">Father Name:</label>
                <input type="text" class="form-control" id="father_name"  name="father_name">
              </div>
        </div>

    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="User Name">
                <label for="name">First Name:</label>
                <input type="text" class="form-control" id="first_name"  name="first_name">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="last_name">Last Name:</label>
                <input type="text" class="form-control" id="last_name"  name="last_name">
              </div>
        </div>

    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="cnic">Cnic:</label>
                <input type="text" class="form-control" id="cnic"  name="cnic">
              </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label for="password">Phone Number:</label>
                <input type="text" class="form-control" id="phone_number"  name="phone_number">
              </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="english">English:</label>
                <input type="checkbox" class="form-control" id="english"  name="english">
              </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="math">Math:</label>
                <input type="checkbox" class="form-control" id="math"  name="math">
              </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="science">Science:</label>
                <input type="checkbox" class="form-control" id="science"  name="science">
              </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="statistics">statistics:</label>
                <input type="checkbox" class="form-control" id="statistics"  name="statistics">
              </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="computer_science">Computer Science:</label>
                <input type="checkbox" class="form-control" id="computer_science"  name="computer_science">
              </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="password">Artifical Intelligence:</label>
                <input type="checkbox" class="form-control" id="artifical_intelligence"  name="artifical_intelligence">
              </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label >Machine Learning:</label>
                <input type="checkbox" class="form-control" id="machine_learning"  name="machine_learning">
              </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="urdu">Urdu:</label>
                <input type="checkbox" class="form-control" id="urdu"  name="urdu">
              </div>
        </div>
    </div>

    <button type="submit" class="btn btn-primary" id="submit_form">Submit</button>
    {{-- <button type="submit" class="btn btn-primary draft">Save As Draft</button> --}}
  </form>
</div>

</body>
</html>

<script>
    $(window).on('load', function() {
        $('#myModal').modal('show');
    });
    $(document).ready(function() {
       $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        setInterval(function() {
            console.log($("#email").val() != '');
            if($("#email").val() != '' ){
                checkEmail();
            }

        }, 6000);
    });
    $(document).on('click','#submit_form',function(e){
        if($("#email").val() != '' ){
                checkEmail();
        }
    });
    $(document).on('click','#search_email',function(e){
        e.preventDefault();
        // alert('hello');
        findEmail();

    });
    function findEmail(){
        var email=$("#modal_email").val();
        console.log(email);
        if(email.length > 0 )
        {
            $.ajax({
                url: "{{url('/email/find/')}}" + `/${email}` ,
                type: "POST",
                data:$("#email_found_form").serialize(),
                success: function(response) {
                    console.log(response.emailFound);
                    var result=response.emailFound;
                    if(response.success == true)
                    {
                        $('#myModal').modal('hide');
                        $('#email').val(result.email);
                        $('#first_name').val(result.first_name);
                        $('#last_name').val(result.last_name);
                        $('#father_name').val(result.father_name);
                        $('#phone_number').val(result.phone_number);
                        $('#cnic').val(result.cnic);
                        if(result.subjects_informations.english == "on"){
                            console.log("english value " + result.subjects_informations.english);
                            $('#english').prop('checked', true);
                        }
                        if(result.subjects_informations.math === "on"){
                            console.log("math value " + result.subjects_informations.math);
                            $("#math").prop('checked', true);
                        }
                        if(result.subjects_informations.urdu == "on"){
                            console.log("urdu value " + result.subjects_informations.urdu);
                            $('#urdu').prop('checked', true);
                        }
                        if(result.subjects_informations.machine_learning == "on"){
                            console.log("machine_learning value " + result.subjects_informations.machine_learning);
                            $('#machine_learning').prop('checked', true);
                        }
                        if(result.subjects_informations.science == "on"){
                            console.log("science value " + result.subjects_informations.science);
                            $('#science').prop('checked', true);
                        }
                        if(result.subjects_informations.computer_science == "on"){
                            console.log("computer_science value " + result.subjects_informations.computer_science);
                            $('#computer_science').prop('checked', true);
                        }
                        if(result.subjects_informations.artifical_intelligence == "on"){
                            console.log("artifical_intelligence value " + result.subjects_informations.artifical_intelligence);
                            $('#artifical_intelligence').prop('checked', true);
                        }
                        if(result.subjects_informations.statistics == "on"){
                            console.log("computer_science value " + result.subjects_informations.statistics);
                            $('#statistics').prop('checked', true);
                        }
                    }else{
                        $('#not_found_message').css("display", "block");;
                    }
                }
            });
        }

    }

    function checkEmail(){
        var email=$("#email").val();
        $.ajax({
            url: "{{url('/email/info/')}}" + `/${email}` ,
            type: "POST",
            data:$("#user_information_form").serialize(),
            success: function(response) {
                // console.log(response.message);
                if(response.message == "email found")
                {
                    // alert('update');
                    updateInformation();

                }else{
                    saveInformation();
                    // alert('save');

                }
            }
        });
    }
    function updateInformation(){
        // console.log("upate function");
        var email=$("#email").val();
        $.ajax({
                url: "{{url('/update/info')}}" + `/${email}`,
                type: "POST",
                data: $("#user_information_form").serialize(),
                success: function(response) {
                }
            });
    }
    function saveInformation() {
        // var formData = new FormData($('#user_information_form'));
        // console.log($("#user_information_form").serialize());
        // console.log($("#email").val() != '');
        $.ajax({
            url: "{{route('save.user.information')}}",
            type: "POST",
            data: $("#user_information_form").serialize(),
            success: function(response) {
            }
        });
    }
</script>
