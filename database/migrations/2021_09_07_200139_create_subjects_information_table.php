<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubjectsInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subjects_information', function (Blueprint $table) {
            $table->id();
            $table->string('english')->nullable();
            $table->string('math')->nullable();
            $table->string('science')->nullable();
            $table->string('statistics')->nullable();
            $table->string('urdu')->nullable();
            $table->string('computer_science')->nullable();
            $table->string('artifical_intelligence')->nullable();
            $table->string('machine_learning')->nullable();
            $table->bigInteger('user_information_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subjects_information');
    }
}
