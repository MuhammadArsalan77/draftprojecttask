<?php

namespace App\Http\Controllers;

use App\Models\UserInformation;
use Illuminate\Http\Request;

class UserInfoController extends Controller
{
    public function index(){
        return view('Information');
    }
    public function SaveInformation(Request $request){
        $userInformation=new UserInformation();
        $userInformation->first_name= $request->first_name;
        $userInformation->last_name= $request->last_name;
        $userInformation->father_name= $request->father_name;
        $userInformation->cnic= $request->cnic;
        $userInformation->email= $request->email;
        $userInformation->phone_number= $request->phone_number;
        $userInformation->save();
        $userInformation->SubjectsInformations()->create([
            'english'=> $request->english,
            'urdu'=> $request->urdu,
            'math'  => $request->math,
            'science' => $request->science,
            'statistics' => $request->statistics,
            'computer_science' =>$request->computer_science,
            'artifical_intelligence' => $request->artifical_intelligence,
            'machine_learning' => $request->machine_learning
        ]);
        $userInformation->save();
        return response()->json([
            'success' => 'true'
        ]);
    }
    public function checkEmail($email)
    {
        //  dd($email);
        $userInformation=UserInformation::where('email',$email)->get();
        //  dd(count($userInformation));
        if(count($userInformation) > 0)
        {
            return response()->json([
                'success' => 'true',
                'message' => 'email found'
            ]);
        }
        else
        {
            return response()->json([
                'success' => 'false',
                'message' => 'email not found'
            ]);
        }
    }
    public function UpdateInformation($email,Request $request){
        $userInformation=UserInformation::where('email',$email)->with('SubjectsInformations')->first();
        $userInformation->first_name= $request->first_name;
        $userInformation->last_name= $request->last_name;
        $userInformation->father_name= $request->father_name;
        $userInformation->cnic= $request->cnic;
        $userInformation->email= $request->email;
        $userInformation->phone_number= $request->phone_number;
        $userInformation->save();
        $userInformation->SubjectsInformations->english=$request->english ? $request->english : null;
        $userInformation->SubjectsInformations->urdu=$request->urdu ? $request->urdu : null;
        $userInformation->SubjectsInformations->math=$request->math ? $request->math : null;
        $userInformation->SubjectsInformations->science=$request->science ? $request->science : null;
        $userInformation->SubjectsInformations->statistics=$request->statistics ? $request->statistics : null;
        $userInformation->SubjectsInformations->computer_science=$request->computer_science ? $request->computer_science : null;
        $userInformation->SubjectsInformations->artifical_intelligence=$request->artifical_intelligence ? $request->artifical_intelligence : null;
        $userInformation->SubjectsInformations->machine_learning=$request->machine_learning ? $request->machine_learning : null ;
        $userInformation->SubjectsInformations->save();
        return response()->json([
            'success' => 'true',
            'message' => 'data Updated'
        ]);

    }
    public function findEmail($email)
    {
        $emailFound=UserInformation::where('email',$email)->with('SubjectsInformations')->first();
        if($emailFound)
        {
            return response()->json([
                'success' => true,
                'emailFound' => $emailFound
            ]);
        }
        else{
            return response()->json([
                'success' => false,
                'emailFound' => 'email Not found'
            ]);
        }
    }
}
