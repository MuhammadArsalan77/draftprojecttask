<?php

namespace App\Http\Controllers;

use App\Jobs\fakeUser;
use App\Models\User;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class QueueController extends Controller
{
    public function createUser(){
        return view('create-user');
    }
    public function UserCreated(Request $request){
        // dd($request->all());
        fakeUser::dispatch();
        return redirect()->back()->with('done','message');
    }
    public function showUser(){

        return view('show-user');
    }
    public function searchUser(){
        $searchQuery=User::all();
       return Datatables::of($searchQuery)

            ->addColumn('action', function ($row) {
                $returnHtml = '<div class="d-flex justify-content-center">';
                $returnHtml .= '<div class="btn-group d-flex justify-content-center" role="group" aria-label="Action Buttons">';
                $returnHtml .= '<a title="View details." type="button" class="btn btn-sm btn-primary p-2 px-3"><i class="fa fa-info-circle"></i></a>';
                $returnHtml .= '<a title="Edit details." type="button" class="btn btn-sm btn-secondary p-2 px-3" href="' . route('delivery.edit', ['id' => $row->id]) . '"><i class="fa fa-edit"></i></a>';
                $returnHtml .= '<button title="Remove." type="button" class="btn btn-sm btn-danger p-2 px-3"><i class="fa fa-remove"></i></button>';
                $returnHtml .= '</div>';
                $returnHtml .= '</div>';
                return $returnHtml;
            })
            ->setRowId('id')
            ->make(true);
    }
}
