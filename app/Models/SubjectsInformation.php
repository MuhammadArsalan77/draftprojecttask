<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubjectsInformation extends Model
{
    use HasFactory;
    protected $fillable = [
        'english',
        'math',
        'science',
        'statistics',
        'urdu',
        'computer_science',
        'artifical_intelligence',
        'machine_learning'
    ];
    public function UserInformation(){
        return $this->belongsTo(UserInformation::class,'user_information_id','id');
    }
}
