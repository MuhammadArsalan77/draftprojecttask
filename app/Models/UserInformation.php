<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserInformation extends Model
{
    use HasFactory;
    protected $fillable = [
        'first_name',
        'last_name',
        'cnic',
        'email',
        'father_name',
        'phone_number'
    ];
    public function SubjectsInformations(){
        return $this->hasOne(SubjectsInformation::class,'user_information_id','id');
    }
}
