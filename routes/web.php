<?php

use App\Http\Controllers\QueueController;
use App\Http\Controllers\UserInfoController;
use Illuminate\Support\Facades\Route;
use App\Jobs\fakeUser;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/',[UserInfoController::class,'index'])->name('user.Information');

Route::post('/save/info',[UserInfoController::class,'SaveInformation'])->name('save.user.information');

Route::post('/update/info/{email}',[UserInfoController::class,'UpdateInformation'])->name('update.user.information');

Route::post('/email/info/{email}',[UserInfoController::class,'checkEmail'])->name('email.user.information');

Route::post('/email/find/{email}',[UserInfoController::class,'findEmail'])->name('email.user.email');

Route::get('/create/user',[QueueController::class,'createUser'])->name('create.user.queue');

Route::post('/user/created',[QueueController::class,'UserCreated'])->name('user.created');

Route::get('/show/user',[QueueController::class,'showUser'])->name('show.user');
Route::get('/search/user',[QueueController::class,'searchUser'])->name('search.user');
